#!/usr/bin/env bash

baseurl="ssh://aur@aur.archlinux.org"
pkgnames=(
    "activitywatch-git"
    "python-persist-queue")

for pkg in "${pkgnames[@]}"; do
  remote="aur-$pkg"

  read -r -d '\n' url rc < <(git remote get-url "$remote" 2> /dev/null; printf "%d" "$?")
  # When the command fails there is no output, so the return code gets read as the url
  [[ -z "$rc" ]] && rc=$url

  exp_url="$baseurl/$pkg.git"
  if [[ "$rc" -eq 2 ]]; then
    git remote add "$remote" "$exp_url"
    continue
  elif [[ "$rc" -ne 0 ]]; then
    >&2 printf "error: unexpected error when checking for remote %s: %d\n" "$remote" "$rc"
    continue
  fi

  [[ "$url" == "$exp_url" ]] && continue

  >&2 printf "warning: changing url for remote %s from '%s' to '%s'\n" "$remote" "$url" "$exp_url"
  git remote set-url "$remote" "$exp_url"
done
